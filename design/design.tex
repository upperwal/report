% This file includes the design

\section{Design}

% explain terms like job, rank, node, replication map, process manager etc
% Talk about process manager.
% Two thread sync for replica creation.

\textit{\textbf{EntangledMPI}} uses underlying MPI framework for almost everything. MPI profiling layer is used to modify \textit{MPI\_*} functions and ULFM constructs are used to handle process failure. 

It has its own implementation of process replication which helps in process image migration from one node to another and checkpointing which supports process context checkpointing into NFS and starting from the last checkpoint on total failure.

Framework also has a fault injector and a process manager simulator which updates replication map at regular interval.  

\begin{figure}[h]
	\centering
  	\includegraphics[width=0.8\linewidth]{design/images/over_all_design.eps}
  	\caption{Overall Design}
  	\label{fig:overall_design}
\end{figure}

\subsection{Virtual Memory}

User's MPI program is linked dynamically to the library which creates different segments in the virtual memory for different libraries loaded for the program. This differentiate user's address space from any other library.

This way we get three separate virtual memory segments which is only used to store user's data. Segregating virtual memory segments helps in process image migration during replication and checkpointing. No data from EntangledMPI or underlying MPI framework is copied across the process.

\subsection{Process Replication}

As seen in fig. \ref{fig:rep_map} MPI ranks are combined into a job and each job is assigned a rank by the framework. The end user of the framework will now see this job rank instead of the original rank. 

From now on we call the ranks given by the framework as "rank" and the ranks given by underlying MPI framework as "original rank". It should also be noted that if a user executes \textit{MPI\_Comm\_size} it will return "6" and not "9" for the example given in fig. \ref{fig:rep_map}. 

Hence \textit{EntangledMPI} has wrapped around the underlying MPI framework and modified the \textit{MPI\_COMM\_WORLD} to support replication.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.86\linewidth]{design/images/rep_comms.eps}
	\caption{Real Rank to Compute and Replica mapping}
	\label{fig:rep_map}
\end{figure}

The framework internally uses three sets of MPI communicators.

\subsubsection{MPI\_COMM\_WORLD Duplicate}

A duplicate communicator of \textit{MPI\_COMM\_WORLD} is used as a global communicator. This is important as \textit{MPI\_COMM\_WORLD} in most of the MPI implementation is a macro and not a variable hence it is not possible to assign value to \textit{MPI\_COMM\_WORLD} when the communicator is shrunk after process failure.

\subsubsection{Compute Group Communicator}

This communicator as seen in fig. \ref{fig:rep_map} is the group of all compute nodes (one from each job) and is responsible to do MPI function calls within compute nodes only.

\subsubsection{Job Communicator}

This communicator as seen in fig. \ref{fig:rep_map} wraps a compute and all its replica nodes within a job. This communicator is used to send data from compute nodes to replicas once master compute nodes are done with the MPI\_* function.

\subsection{Process Image Migration}

There is a need to periodically update the replication map of a running application to account for future failures. During this the process image is migrated from one node to another. Process migration requires the virtual memory segments ie. \textbf{Data Segment}, \textbf{Stack Segment} and \textbf{Heap Segment} of one process to be copied to another process. This should happen every time the replication map is updated by the process manager.

To enable this both the processes (sender and receiver of the image) should have identical virtual memory mapping ie. $Address\ Space\ Layout\ Randomisation$ should be disabled on all the nodes. 

\subsubsection{Data Segment Migration}

Data segment is the memory where global \textit{initialized} and \textit{uninitialized} variables reside. Memory is allocated and mapped to a variable name during compilation and this mapping does not change during program execution.

Variables initialized with a non zero value goes in the initialized segment and variables uninitialized or initialized with "0" goes to uninitialized segment.

On looking at the symbol table of an executable (using $nm$) one can find four symbols associated with the data segment which are of interest. $\_\_data\_start$ marks the start of initialized data section, $\_edata$ is the end of initialized data section, $\_\_bss\_start$ points at the start of uninitialized data section and $\_end$ is the end of uninitialized data section. These symbol cannot be used as variables instead they are only used to extract the boundaries of memory associated with them using \& operator. 

$$void\ *a = \&\_\_data\_start;$$

Migrating $Data\ Segment$ is easy as it only involves copying memory between $\_\_data\_start$ and $\_edata$ ie. initialized data section and $\_edata$ to $\_end$ ie. uninitialized data section to a new process. This is done using normal MPI construct (\textit{MPI\_Send} and \textit{MPI\_Recv})

This part of the virtual memory will only have global variables defined in the user's program.

\subsubsection{Stack Segment Migration}

Stack migration is also identical to Data Segment but we need to determine the start and end of the virtual memory to copy. 

\subsubsection{Heap Segment Migration}

\subsection{Replica Map Change}

\subsection{Redesigning MPI functions to support Replication}

It is important to maintain identical data across all replicated nodes during the program execution. To achieve this \textit{EntangledMPI} uses simple procedures for collective and non collective calls. These procedures help \textit{EntangledMPI} maintain identical data after every \textit{MPI\_*} function call from the user's program. It should be noted that these procedures do not handle process failures which is explained separately in ULFM section.

\subsubsection{Collective Calls}

Fig. \ref{fig:collective_design} shows an example of how a collective calls happens with replication. In this example \textit{MPI\_Scatter} is called with root as "0". Remember in this example if the user executes \textit{MPI\_Comm\_size} it will return "4" with ranks ranging from 0 to 3. Where \textit{"C1"} and \textit{"R1"} represents rank "0" and so on.

Collective calls with replication is a two step process. In the first step \textit{MPI\_*} call happens only with the compute node group which uses \textit{Compute Group Communicator} and in the second step each compute node sends the data to its corresponding replicas using \textit{Job Communicator}.

\begin{figure}[h]
	\includegraphics[width=\linewidth]{design/images/collective_design.eps}
	\caption{Collective Calls}
	\label{fig:collective_design}
\end{figure}

\subsubsection{Non Collective Calls}

As non collective calls does not necessarily include all the nodes in a communicator. It's implementation is slightly different than collective call. Other reason for a different algorithm is the mechanism to handle process failure using ULFM which uses all nodes in a communicator.

\begin{figure*}[h]
	\centering
	\includegraphics[width=0.9\linewidth]{design/images/non_collective_design.eps}
	\caption{Non Collective Calls}
	\label{fig:non_collective_design}
\end{figure*}

Instead of using \textit{Job Comm} and \textit{Master Compute Comm} it relies on \textit{MPI\_COMM\_WORLD duplicate comm} to send one-to-one messages to all the necessary nodes. Fig. \ref{fig:non_collective_design} shows how messages are send for three different cases.

In the first case both the jobs have a replica hence duplicate messages are send to both compute and replica of the receiving job.

In the second case only sender has a replica hence both compute and replica of sender job (rank "0") will send message to the compute node (rank "1"). Note that the data/message send is identical as compute and replica are computing on identical instructions and data.

Case three is similar to case two but instead of sender the receiver has a replica. So now the compute node of the sender will be responsible to sender data to both compute and replica of the receiver.

There is another case where none has a replica which boils down to normal MPI point-to-point communication between compute nodes.

\subsubsection{Optimizing \textit{MPI\_Bcast}}

Although \textit{MPI\_Bcast} could follow collective call procedure for replication. We could optimize the procedure by using single step instead of two. 

So instead of first sending the data to all master compute nodes we directly broadcast it to \textit{MPI\_COMM\_WORLD duplicate}. Replicas of "root" rank job  will receive duplicate data which they can ignore and proceed but all other compute and replicas will receive the correct data in one go.

With this approach we directly use the underlying \textit{MPI\_Bcast} which is highly optimized.

This works because \textit{MPI\_bcast} satisfies two conditions. Firstly, identical data needs to be sent to all the nodes in a communicator, secondly \textit{MPI\_Bcast} does not depend on any other node to source the data ie. the data to be send is available on a single node. Second condition is important because functions like \textit{MPI\_Allgather} could satisfy the first condition but is not ideal for this optimization.

\subsubsection{Handling non-blocking MPI calls}

MPI functions like \textit{MPI\_Isend} and \textit{MPI\_Irecv} are very useful in situations where data is not required immediately. But this also brings new challenges in implementing a replication framework. 

First problem is of a usable or valid buffer after \textit{MPI\_Isend} and \textit{MPI\_Irecv} are called. It is safe to reuse the buffer only after \textit{MPI\_Wait} is successfully executed. But if we migrate or checkpoint the process image before \textit{MPI\_Wait} is executed the buffer will contain corrupted data which can lead to segmentation fault and data corruption. Hence it is important to keep track of all the request generated from \textit{MPI\_Isend} and \textit{MPI\_Irecv} and only checkpoint or migrate once all the requests are completed.

Second problem is to track internal request for each compute and replicas during \textit{MPI\_Isend} and \textit{MPI\_Irecv}. As shown in fig. \ref{fig:req} this is done by generating a unique \textit{MPI\_Request} for each internal \textit{PMPI\_Isend} and \textit{PMPI\_Irecv} calls and creating a linked-list for all such requests. Head of this linked-list is stored in \textit{MPI\_Request} supplied by the user. 

So \textit{MPI\_Wait} should wait for all internal requests to complete one \textit{MPI\_Isend} and \textit{MPI\_Irecv}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{design/images/req.eps}
	\caption{Handling requests on non-blocking calls}
	\label{fig:req}
\end{figure}

\subsubsection{Handling MPI\_ANY\_SOURCE}

Problem associated with \textit{MPI\_ANY\_SOURCE} is that the framework does not know the number of \textit{PMPI\_Recv} or \textit{PMPI\_Irecv} to do internally beforehand as this number depends on the number of worker nodes associated with the "source" job.

If \textit{PMPI\_Recv} or \textit{PMPI\_Irecv} is called less or more number of times as compared to the number of nodes sending data, the sender or receiver could result in a deadlock state.

Solution to this problem is to perform one \textit{PMPI\_Recv} or \textit{PMPI\_Irecv} and track the request till completion. On completion status will give \textit{MPI\_SOURCE} using which we can obtain the number of nodes ($n$) associated with a job. We can then perform $n - 1$ \textit{PMPI\_Recv} or \textit{PMPI\_Irecv} to get the data from remaining nodes.

Working on same thread could work for \textit{PMPI\_Recv} but for \textit{PMPI\_Irecv} a new thread should be spawned to track the first request and do \textit{PMPI\_Irecv} $n - 1$ times. Rest can be taken care by \textit{MPI\_Wait}.