% This file includes ulfm related content

\section{Integrating ULFM}

User Level Fault Mitigation (ULFM) is used in this framework to detect process failures and act on it. By using ULFM we get functions like \textit{MPI\_Comm\_shrink}, \textit{MPI\_Comm\_agree} and all MPI functions return an additional error \textit{MPI\_ERR\_PROC\_FAILED} on process failure.

The main challenge and focus of this work is to use ULFM efficiently and redesign MPI functions with inherent fault tolerance but \textbf{still using the underlying MPI algorithms}. So even though we rewrite \textit{MPI\_Scatter} to support process replication and failure \textbf{we use profiling layer} to take advantage of underlying algorithm of \textit{MPI\_Scatter}. This way we get the best of MPI with process replication and failure support.

\subsection{Custom MPI Error Handler for Process Failure}

This framework implements a custom MPI error handler which is used to handle process failures. It uses ULFM to acknowledge the failures, shrink the global communicator and rebuild \textit{Job Communicator}, \textit{Compute Group Communicator} and create new \textit{MPI\_COMM\_WORLD duplicate}.

\begin{algorithm}[h]
  \caption{Algorithm to handle process failure}
  \label{alg::failure}
  \begin{algorithmic}[1]
  \Require
  		old\_comm
  	\If{error\_type := MPI\_ERR\_PROC\_FAILED \&\& comm( MPI\_COMM\_WORLD ) \&\& func\_active( COLLECTIVE )}
	    \State MPI\_Comm\_failure\_ack(old\_comm) 
	    \State MPI\_Comm\_failure\_get\_acked(old\_comm, failed\_group)
	    \State MPI\_Comm\_shrink(old\_comm, new\_comm)
	    \State new\_ranks = translate\_rank(old\_comm, new\_comm)
	    \If{rank\_count(job\_i) == 0}
	    	\State MPI\_Abort("Insufficient ranks for job i")
	    \EndIf
	    \State update\_job\_list(new\_ranks)
	    \State update\_comms()
	\EndIf
  \end{algorithmic}
\end{algorithm}

Algorithm \ref{alg::failure} shows how this custom error handler works. It should be noted that this algorithm only \textbf{works for global communicator} (duplicate) on \textbf{detecting a process failure} and only if the error was detected during a \textbf{collective call}.

Framework ignores all other errors except \textit{MPI\_ERR\_PROC\_FAILED} and returns the error as if MPI error handler was set to \textit{MPI\_ERRORS\_RETURN}. We only catch process failure errors for global communicator as all other communicators will be a subset of global communicator and errors for them will be taken care implicitly. 

\begin{figure}
	\includegraphics[width=\linewidth]{ulfm/images/err_handler.eps}
	\caption{Error Handler on Process Failure: On C4 and C7 failure they are removed after communicator shrinking and new job and compute communicator is formed where R4 and R7 (their replicas) takes over and are new compute nodes for rank "4" and "7".}
	\label{fig:err_handler}
\end{figure}

Algorithm (alg. \ref{alg::failure}) is pretty simple, it starts by acknowledging the failed processes on old communicator using \textit{MPI\_Comm\_failure\_ack} and extracting the group of failed nodes from old communicator using \textit{MPI\_Comm\_failure\_get\_acked}. This group of failed nodes is send to the process manager to exclude the failed nodes from replication map in the next round of map update.

\textit{MPI\_Comm\_shrink} is used to shrink the old communicator and form a new communicator which now excludes the dead nodes. New communicator should now be our global communicator.

We use this new communicator to translate ranks from old communicator to new communicator and update our internal job mappings. It is also used to form new \textit{Job Communicator} and \textit{Compute Group Communicator}

A case could also be encountered where all nodes allocated to a job dies (i.e. no compute and replicas for a job). In this case MPI\_Abort is called after rank transaction.

\subsection{Collective Calls with ULFM}

As most of the heavy lifting after process failure is done by the error handler, MPI functions only need to check whether the underlying PMPI constructs have completed successfully or not in all the nodes in the given communicator.

As described above collective call with replication is a two step process where in the first we do the MPI operation only with \textit{Compute Group Communicator} and later send the result to respective replicas using \textit{Job Communicator}.

Now we will also add support for process failure in the above procedure. This modification is shown in fig. \ref{fig:scatter} where we take an example of \textit{MPI\_Scatter} on \textit{MPI\_COMM\_WORLD} (remember we are still using a duplicate of \textit{MPI\_COMM\_WORLD} in the framework). Refer to algorithm \ref{alg::coll_call_failure} to see it's working in detail. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{ulfm/images/scatter.eps}
	\caption{Collective call example with ULFM support}
	\label{fig:scatter}
\end{figure}

\subsubsection{\textbf{Step 1:} Doing MPI\_Scatter operation within compute communicator}

We start by doing an \textit{MPI\_Scatter} with \textit{Compute Group Communicator} and then check for agreement on the status returned by \textit{MPI\_Scatter} across all the nodes in \textit{Compute Group Communicator} using \textit{MPI\_Comm\_agree}. Process failure could occur either during \textit{MPI\_Scatter} or \textit{MPI\_Comm\_agree}.

If process failure occurs, error handler will be invoked which would shrink the global communicator, form new \textit{Job Communicator} and \textit{Compute Group Communicator}. Although the communicators are repaired \textit{MPI\_Scatter} will still \textbf{return a failure} (i.e. status != MPI\_SUCCESS) because the actual scatter operation is still not complete.

There could be a case where for some nodes \textit{MPI\_Scatter} returns \textit{MPI\_SUCCESS} and for others it returns a failure. This could happen if process failure occur while the scatter algorithm is in the middle. In this case the nodes which received \textit{MPI\_SUCCESS} for scatter will detect process failure during \textit{MPI\_Comm\_agree} call and others will detect it during \textit{MPI\_Scatter} itself.

For the nodes which detect process failure during \textit{MPI\_Comm\_agree} call error handler will be invoked and communicator will be shrink and repaired same as earlier but now \textit{MPI\_Comm\_agree} will return an error (i.e. status != MPI\_SUCCESS) as agree operation is still incomplete. We use a loop to detect all such errors at this step and the program will only proceed if all the nodes in \textit{Compute Group Communicator} agree on success of the \textit{MPI\_Scatter} operation. In case of non agreement all the nodes in \textit{Compute Group Communicator} will do \textit{MPI\_Scatter} again until all the nodes agree on the success.

It should be noted that although it looks like an infinite loop in case of multiple process failures but the framework supports a threshold value for number of trial before aborting.

At this point it is sure that \textit{MPI\_Scatter} operation is successful on all the nodes in \textit{Compute Group Communicator} and compute nodes can now send the data to corresponding replicas.

One important thing to note at this point is that each compute node holds a piece of data that is unique to a given job. If this node dies at this point the data is lost with it.

\subsubsection{\textbf{Step 2:} Sending data to the corresponding replicas}

In this step all compute nodes will send the data received in step 1 to their corresponding replicas. \textit{MPI\_Bcast} is used to send data from compute to replica process. Using \textit{MPI\_Bcast} the framework can support multiple replicas for a single job and compute node is used as "root" in \textit{Job Communicator}.

\begin{algorithm}[h]
  \caption{Collective Call Algorithm with Process Replication and Failure}
  \label{alg::coll_call_failure}
  \begin{algorithmic}[1]
    \State success = MPI\_SUCCESS  
    \State is\_replication\_map\_updated()
    \State acquire\_comm\_lock()
    \Repeat
    	\State // Do MPI call across Active Master Group
    	\State status = PMPI\_*(buf, ..., active\_master\_comm, ...)
    	\State success = (status == MPI\_SUCCESS)
    	\While{!status} 
    		\State // Over MPI\_COMM\_WORLD
    		\State status = PMPI\_Comm\_agree(... , success)
    	\EndWhile
    \If{!success}
    	\State success = 0
    	\State continue
    \Else
    	\Repeat
    		\State // Send the data to replicas
    		\State PMPI\_Bcast(buf, ..., job\_comm, ...)
    		\State success = (status == MPI\_SUCCESS)
	    	\While{!status} 
	    		\State // Over MPI\_COMM\_WORLD
	    		\State status = PMPI\_Comm\_agree(... , success)
	    	\EndWhile
	    	\If{!success \&\& failed\_is\_master()}
		    	\State success = 0
		    	\State break
		    \EndIf
    	\Until{!success}
    \EndIf
    \Until{!success}
    \State release\_comm\_lock()
    \State return success
  \end{algorithmic}
\end{algorithm}

Process failure is handled in a similar manner as in step 1 where consensus is made using \textit{MPI\_Comm\_agree} over status of \textit{MPI\_Bcast}. Similarly, process failure can occur at \textit{MPI\_Bcast} or \textit{MPI\_Comm\_agree} hence a while loop is used for \textit{MPI\_Comm\_agree}.

Although the process is similar there is one case which should be taken care. As discussed above the compute node has data which is unique to a given job. If this compute node dies the data is also lost. To consider this situation step 2 has two kinds of process failures as shown in fig. \ref{fig:scatter} and explained below.

\textit{Any replica node dies}: In this case error handler takes over and repair all communicators. Nothing extra should be done and compute node will send the data to the remaining alive replica nodes.

\textit{Compute node dies}: If the dead node is compute node, step 1 should be executed again because the data is also lost when compute node died. Error handler will repair the communicators and assign one of the replica as new compute node as seen in fig. \ref{fig:err_handler} (which is also added to \textit{Compute Group Communicator}). All the processes in \textit{Compute Group Communicator} will do step 1 again and proceed to step 2 once every node in compute group succeeds.

\subsection{Non Collective Calls}

As non-collective calls does not necessarily include all the nodes in a communicator, process failure during non-collective call is ignored. Communicator will still be valid and MPI messages can be send to any non faulty nodes. Communicator repairing will be postponed to any collective call in the future.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\linewidth]{ulfm/images/non_coll.eps}
	\caption{Non Collective call during process failure}
	\label{fig:non_coll}
\end{figure}

Fig. \ref{fig:non_coll} shows how point to point communication succeeds even if process failure is ignored. As the communicator is valid after process failure when "C1" sends (\textit{MPI\_Send}) data to "C2", \textit{MPI\_Send} will not succeed and returns \textit{MPI\_ERR\_PROC\_FAILED}. Error handler will be invoked but it will ignore the process failure.

"C1" will simply ignore this error and proceed to send data to "R2". Same would happen when "R1" sends data to "C2".

By ignoring the process failure during non-collective call we try to reduce the overhead of communicator shrinking and repair. As process failure does not interfere with point to point communication it is safe to ignore it at this point. Communicator shrinking and repair would happen in the very next collective call.

\subsection{Special case for \textit{MPI\_Bcast}}

As discussed while implementing \textit{MPI\_Bcast} for replication, we can optimize \textit{MPI\_Bcast} by doing only one step instead of two. 

\begin{algorithm}[h]
  \caption{Optimized MPI\_Bcast with Process Replication and Failure}
  \label{alg::bcast_opt}
  \begin{algorithmic}[1]
    \State success = MPI\_SUCCESS  
    \State is\_replication\_map\_updated()
    \State acquire\_comm\_lock()
    \Repeat
    	\State // Over MPI\_COMM\_WORLD
    	\State status = PMPI\_Bcast(buf, ..., MPI\_COMM\_WORLD)
    	\State success = (status == MPI\_SUCCESS)
    	\While{!status} 
    		\State // Over MPI\_COMM\_WORLD
    		\State status = PMPI\_Comm\_agree(... , success)
    	\EndWhile
    \If{!success}
    	\State success = 0
    	\State continue
    \EndIf
    \Until{!success}
    \State release\_comm\_lock()
    \State return success
  \end{algorithmic}
\end{algorithm}

Algorithm \ref{alg::bcast_opt} shows how \textit{MPI\_Bcast} works with this optimization. We only use step 1 of the original algorithm to do the broadcast over MPI\_COMM\_WORLD (using duplicate of MPI\_COMM\_WORLD internally) and do the agreement after that. Even though the replicas of this job will receive redundant data they can simply ignore it and proceed.

% \begin{algorithm}[h]
%   \caption{Conjugate Gradient Algorithm with Dynamic Step-Size Control}
%   \label{alg::conjugateGradient}
%   \begin{algorithmic}[1]
%     \Require
%       $f(x)$: objective funtion;
%       $x_0$: initial solution;
%       $s$: step size;
%     \Ensure
%       optimal $x^{*}$    
%     \State initial $g_0=0$ and $d_0=0$;
%     \Repeat
%       \State compute gradient directions $g_k=\bigtriangledown f(x_k)$;
%       \State compute Polak-Ribiere parameter $\beta_k=\frac{g_k^{T}(g_k-g_{k-1})}{\parallel g_{k-1} \parallel^{2}}$;
%       \State compute the conjugate directions $d_k=-g_k+\beta_k d_{k-1}$;
%       \State compute the step size $\alpha_k=s/\parallel d_k \parallel_{2}$;
%     \Until{($f(x_k)>f(x_{k-1})$)}
%   \end{algorithmic}
% \end{algorithm}