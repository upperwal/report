all: EntangledMPI.pdf

EntangledMPI.pdf: EntangledMPI.dvi
	dvipdf EntangledMPI.dvi

EntangledMPI.dvi: EntangledMPI.tex abstract/abstract.tex intro/intro.tex background/background.tex design/design.tex ulfm/ulfm.tex checkpoint/checkpoint.tex expres/expres.tex future/future.tex EntangledMPI.bib abstract/abstract.bib background/background.bib design/design.bib intro/intro.bib ulfm/ulfm.bib checkpoint/checkpoint.bib
	latex EntangledMPI.tex 
	bibtex EntangledMPI
	latex EntangledMPI.tex
	latex EntangledMPI.tex

clean:
	rm -f *.aux *.bbl *.blg *.dvi *.log *.pdf
